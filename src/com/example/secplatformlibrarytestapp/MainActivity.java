package com.example.secplatformlibrarytestapp;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Calendar;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.buildj.seclabplatform.ASN1Encoding;
import com.buildj.seclabplatform.AsymmetricKeys;
import com.buildj.seclabplatform.Certificate;
import com.buildj.seclabplatform.DistinguishedName;
import com.buildj.seclabplatform.Hash;
import com.buildj.seclabplatform.PKCS7;
import com.buildj.seclabplatform.PrivateKey;
import com.buildj.seclabplatform.PublicKey;
import com.buildj.seclabplatform.SMIME;
import com.buildj.seclabplatform.SecurityPlatformSpongyCastle;
import com.buildj.seclabplatform.SymmetricKey;
import com.buildj.seclabplatform.utils.Constants;
import com.buildj.seclabplatform.utils.PlatformUtils;

public class MainActivity extends Activity {
	LinearLayout hashLayout;
	LinearLayout hashFunctions;
	TextView hashHeader;
	TextView type1;
	TextView type2;
	TextView type3;
	SecurityPlatformSpongyCastle castleTest;
	TextView output;
	TextView outputheader;

	///////// Testing Parameters
	String plainText = "This message is used for testing";
	byte[] input_byte = plainText.getBytes();
	String password = "password";
	String encrypted;
	String decrypted;
	String seed = "ThisStringIsSeed";
	String salt = "ThisStringIsSalt";
	String serialized_symm_key;
	Object obj;
	String salted_symm_key;
	String enveloped_symm_key;
	String deEnveloped_symm_key;
	long startingTime= 0;
	long endTime=0;
	LinearLayout hashHeaderLayout;
	ImageView back;
	ImageView forward;
	private String MODE;
	private final String HASH_MODE = "HASH";
	private final String ASYM_MODE = "ASYM";
	private final String SYM_MODE = "SYM";
	private final String DNF_MODE ="DNF";
	private final String CERT_MODE ="CERT";
	String outputText= "";
	static DistinguishedName subject1 = new DistinguishedName("US", // countryName
			"D.C.", // stateOrProvinceName
			"Washington", // locality
			"GWU", // organizationName
			"SEAS", // organizationalUnitName
			"Admin1", // commonName
			"", // emailAddress
			""); // URL (for Servers)

	static DistinguishedName subject2 = new DistinguishedName("US", // countryName
			"D.C.", // stateOrProvinceName
			"Washington", // locality
			"GWU", // organizationName
			"SEAS", // organizationalUnitName
			"Admin2", // commonName
			"", // emailAddress
			""); // URL (for Servers)

	static DistinguishedName subject3 = new DistinguishedName("US", // countryName
			"D.C.", // stateOrProvinceName
			"Washington", // locality
			"GWU", // organizationName
			"SEAS", // organizationalUnitName
			"Admin3", // commonName
			"", // emailAddress
			""); // URL (for Servers)

	static DistinguishedName issuer = new DistinguishedName("US", // countryName
			"D.C.", // stateOrProvinceName
			"Washington", // locality
			"GWU", // organizationName
			"SEAS", // organizationalUnitName
			"OneNETServer35", // commonName
			"", // emailAddress
			"128.164.82.35"); // URL (for Servers


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		castleTest = new SecurityPlatformSpongyCastle();
		hashLayout = (LinearLayout)findViewById(R.id.hashLayout);
		hashHeader = (TextView)findViewById(R.id.hashHeader);
		hashHeaderLayout =(LinearLayout)findViewById(R.id.hashHeaderLayout);
		hashFunctions =(LinearLayout)findViewById(R.id.hashFunctions);
		back =(ImageView)hashHeaderLayout.findViewById(R.id.back);
		back.setVisibility(View.GONE);
		forward =(ImageView)hashHeaderLayout.findViewById(R.id.forward);
		type1 = (TextView)findViewById(R.id.SHA128);
		type2 = (TextView)findViewById(R.id.SHA_256);
		type3 = (TextView)findViewById(R.id.SHA_512);
		output =(TextView)findViewById(R.id.output);
		outputheader =(TextView)findViewById(R.id.header);
		//MODE =HASH_MODE;
		MODE = CERT_MODE;
		buildCertLayout();
		//buildHashFunctionsLayout(Constants.SHA224);

		forward.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (MODE) {
				case HASH_MODE:
					MODE = SYM_MODE;
					hashHeader.setText("SYMMETRIC FUNCTIONS");
					type1.setText(Constants.AES128);
					type2.setText(Constants.AES192);
					type3.setText(Constants.AES256);
					back.setVisibility(View.VISIBLE);
					forward.setVisibility(View.VISIBLE);
					buildSymFunctionsLayout(Constants.AES_128);
					break;
				case SYM_MODE:
					MODE = ASYM_MODE;
					hashHeader.setText("ASYMMETRIC FUNCTIONS");
					type1.setText(Constants.RSA1024);
					type2.setText(Constants.RSA2048);
					type3.setText(Constants.RSA3092);
					back.setVisibility(View.VISIBLE);
					forward.setVisibility(View.VISIBLE);
					buildAsymFunctionsLayout(Constants.RSA_1024);
					break;
				case ASYM_MODE:
					MODE = DNF_MODE;
					back.setVisibility(View.VISIBLE);
					forward.setVisibility(View.VISIBLE);
					hashHeader.setText("DISTINGUISHED NAME");
					hashFunctions.setVisibility(View.GONE);
					buildDNFLayout();
					break;

				case DNF_MODE :
					MODE =CERT_MODE;
					back.setVisibility(View.VISIBLE);
					forward.setVisibility(View.GONE);
					hashHeader.setText("CERT");
					hashFunctions.setVisibility(View.GONE);
					buildCertLayout();
				default:
					break;
				}

			}
		});

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (MODE) {
				case SYM_MODE:
					MODE = HASH_MODE;
					back.setVisibility(View.GONE);
					forward.setVisibility(View.VISIBLE);
					hashHeader.setText("HASH FUNCTIONS");
					type1.setText(Constants.SHA_224);
					type2.setText(Constants.SHA_256);
					type3.setText(Constants.SHA_512);
					buildHashFunctionsLayout(Constants.SHA224);
					break;
				case ASYM_MODE:
					MODE = SYM_MODE;
					hashHeader.setText("SYMMETRIC FUNCTIONS");
					type1.setText(Constants.AES128);
					type2.setText(Constants.AES192);
					type3.setText(Constants.AES256);
					back.setVisibility(View.VISIBLE);
					forward.setVisibility(View.GONE);
					buildSymFunctionsLayout(Constants.AES_128);
					break;
				case DNF_MODE:
					MODE = ASYM_MODE;
					hashHeader.setText("ASYMMETRIC FUNCTIONS");
					hashFunctions.setVisibility(View.VISIBLE);
					type1.setText(Constants.RSA1024);
					type2.setText(Constants.RSA2048);
					type3.setText(Constants.RSA3092);
					back.setVisibility(View.VISIBLE);
					forward.setVisibility(View.VISIBLE);
					buildAsymFunctionsLayout(Constants.RSA_1024);
					break;
				case CERT_MODE :
					MODE = DNF_MODE;
					back.setVisibility(View.VISIBLE);
					forward.setVisibility(View.VISIBLE);
					hashHeader.setText("DISTINGUISHED NAME");
					buildDNFLayout();
				default:
					break;
				}



			}
		});



		type1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(MODE.equals(HASH_MODE)){
					buildHashFunctionsLayout(Constants.SHA224);
				}
				if(MODE.equalsIgnoreCase(SYM_MODE)){
					buildSymFunctionsLayout(Constants.AES_128);
				}
				if(MODE.equals(ASYM_MODE)){
					buildAsymFunctionsLayout(Constants.RSA_1024);
				}

			}
		});
		type2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(MODE.equals(HASH_MODE)){
					buildHashFunctionsLayout(Constants.SHA256);
				}
				if(MODE.equalsIgnoreCase(SYM_MODE)){
					buildSymFunctionsLayout(Constants.AES_192);
				}

				if(MODE.equals(ASYM_MODE)){
					buildAsymFunctionsLayout(Constants.RSA_2048);
				}			}
		});
		type3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(MODE.equals(HASH_MODE)){
					buildHashFunctionsLayout(Constants.SHA512);
				}
				if(MODE.equalsIgnoreCase(SYM_MODE)){
					buildSymFunctionsLayout(Constants.AES_256);
				}
				if(MODE.equals(ASYM_MODE)){
					buildAsymFunctionsLayout(Constants.RSA_3092);
				}

			}
		});
	}
	protected void buildCertLayout() {
		String outputHe = "";
		outputHe += "Creating self signed Certificate";
		outputheader.setText(outputHe);        


		int digSignature = 128;
		int nonRepudation = 64;
		int keyEncipherment =32;
		int dataEncipherment = 16;
		int keyArrangement=8;
		int keyCertSign =4;
		int cRlSign =2;
		int result = -1;

		int usage = digSignature+keyCertSign+cRlSign;
		// --Function 28 : create self-signed root Certificate
		outputText = "Function 28 : create self-signed root Certificate";
		output.setText(outputText);
		startingTime = System.currentTimeMillis();
		Certificate cert =new Certificate();
		result = cert.create_self_signed(getFilesDir()+"", issuer, usage, password);
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (result == 0){
			outputText +="\n********* Self signed Certificate created";
		}
		else{
			outputText +="\n --------> Problems when creating self signed Certificate .... result : "+result;
		}
		output.setText(outputText);


		// Function 29 : create certificate request(PCKS#10)
		outputText +="\nFunction 29 : create certificate request(PCKS#10)";
		startingTime = System.currentTimeMillis();
		result = cert.create_request(getFilesDir()+"", subject1, usage, password);
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (result == 0){
			outputText +="\n********* Certificate Request Creaated";
		}
		else{
			outputText +="\n --------> Problems when creating self signed Certificate request.... result : "+result;
		}
		output.setText(outputText);
		outputText += "\n-----------------------------------------\n"+ 
				"Function 30 : create X.509 certificate out of the certificate request";
		startingTime = System.currentTimeMillis();
		// --Function 30 : create X.509 certificate out of the certificate request
		//Create x509 certificate out of the certificate request
		result = cert.create(getFilesDir()+"", issuer, usage, password);
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (result == 0){
			outputText +="\n********* X509 certificate out of the certificate request is created";
		}
		else{
			outputText +="\n --------> Problems when creating X509 certificate out of the certificate request.... result : "+result;
		}
		output.setText(outputText);
		//Function 31 : receive certificate reply (BKS)
		outputText += "\n-----------------------------------------\n"+ 
				"Function 31 : receive certificate reply (BKS)";

		startingTime = System.currentTimeMillis();
		result =cert.receive_reply(getFilesDir()+"",subject1,password);
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (result == 0){
			outputText +="\n********* Certificate reply received";
		}
		else{
			outputText +="\n --------> Problems when receiving Certificate reply.... result : "+result;
		}
		output.setText(outputText);
		//Function 32 :request certificate from the Certificates DB
		outputText += "\n-----------------------------------------\n"+ 
				"Function 32 :request certificate from the Certificates DB";

		startingTime = System.currentTimeMillis();
		ASN1Encoding encode =cert.request(getFilesDir()+"", subject1, usage);
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (encode.getLength() != 0){
			outputText +="\n********* Certificate lenght = "+ encode.getLength();
		}
		else{
			outputText +="\n --------> Certificate not found in the certificates";
		}
		output.setText(outputText);
		extractCertificate();
		startingTime = System.currentTimeMillis();


		cert = new Certificate(encode);
		result = cert.export_Certificate_to_P7S(getFilesDir()+"");
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (result == 0){
			outputText +="\n********* Certificate in P7S format exported";
		}
		else{
			outputText +="\n --------> Problems when exporting P7S format.... result : "+result;
		}
		output.setText(outputText);
		startingTime = System.currentTimeMillis();
		result = cert.import_Certificate_from_P7S(getFilesDir()+"");
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (result == 0){
			outputText +="\n********* Certificate in P7S format imported";
		}
		else{
			outputText +="\n --------> Problems when importing P7S format.... result : "+result;
		}
		output.setText(outputText);
		startingTime = System.currentTimeMillis();

		result = cert.export_Certificate_to_DER(getFilesDir()+"");
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (result == 0){
			outputText +="\n********* Certificate in DER format exported";
		}
		else{
			outputText +="\n --------> Problems when exporting DER format.... result : "+result;
		}
		output.setText(outputText);
		startingTime = System.currentTimeMillis();
		result = cert.import_Certificate_to_DER(getFilesDir()+"");
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (result == 0){
			outputText +="\n********* Certificate in DER format imported";
		}
		else{
			outputText +="\n --------> Problems when importing DER format.... result : "+result;
		}
		output.setText(outputText);
		/*GetPrivPubKeyCert(encode);
		VerCert(encode);
		SaveCert(encode);
		RevokeCert(encode);
		ExtendCert(encode, password);
		DelCert(subject1);*/
		byte[] signedData =createSignDataPKCS7(password);
		verifySignDataPKCS7(password, signedData);
		signedData=addSignerPKCS7(password, issuer, signedData, null);
		verifyPKCS7(password, signedData);
		output.setText(outputText);
		PKCS7 pkcs7 = new PKCS7();
		DistinguishedName[] signers = pkcs7.getSigners(getFilesDir()+"", signedData);
		outputText += "\n Signers are : \n \n";
		outputText += "***************************\n";
		for(int i =0;i<signers.length;i++){
			outputText += signers[i].toString()+"\n";
		}
		outputText += "***************************\n";
		output.setText(outputText);
		// Extract data from PCKs#7 Signed Data
		byte [] originalData = pkcs7.getOriginalDataFromSignedData(getFilesDir()+"", signedData);
		if(originalData== null){
			outputText += "Error  : Problem when extracting original data";
		}
		else {
			outputText +="Obtaining original data... OK !\n";
			outputText +=PlatformUtils.convertByteArrayToAscString(originalData)+"\n";
		}
		output.setText(outputText);

		DistinguishedName [] recipients = {subject1,subject1};
		outputText += "Creating enveloped Data ...";
		byte[] envolpedData = pkcs7.createEnvelopedData(getFilesDir()+"", originalData, recipients);
		if(envolpedData.length ==0){
			outputText +="Enveloped Data is NULL !\n";
		}
		else{
			outputText +="Creating Enveloped Data .... OK !\n";
			outputText +=PlatformUtils.convertByteArrayToAscString(envolpedData);
		}
		output.setText(outputText);
		byte openData[] = pkcs7.openEnvelopedData(getFilesDir()+"", envolpedData, recipients[0], password);
		if(openData.length != 0){
			outputText += "\nSubject 0 opening enveloped Data ... OK!\n";
			outputText+="Data in the denvelope : \n";
			outputText+=PlatformUtils.convertByteArrayToAscString(openData);
		}
		else {
			outputText+="\nSubject 0 opening enveloped Data is failed~\n";
		}
		output.setText(outputText);
		byte[] signedEvelopedData = pkcs7.createSignedEnvelopedData(getFilesDir()+"", originalData, issuer, password, recipients);
		if(signedEvelopedData.length==0){
			outputText+="\nSignedAndEnvelopedData is NULL!";
		}
		else{
			outputText+= "Creating signedAndEnvelopedData... OK!";
			outputText+= "\n"+PlatformUtils.convertByteArrayToAscString(signedEvelopedData);
		}
		output.setText(outputText);

		byte[] openSandEnvelopedData =pkcs7.openSignedAndEnvelopedData(getFilesDir()+"", signedEvelopedData, subject1, password);
		if(openSandEnvelopedData.length==0){
			outputText+="\nOpening Signed and Enveloped Data  is failed!";
		}
		else{
			outputText+= "Opening signedAndEnvelopedData... OK!";
			outputText+= "\n"+PlatformUtils.convertByteArrayToAscString(openSandEnvelopedData);
		}
		output.setText(outputText);
		SymmetricKey key = new SymmetricKey();
		byte[] encryptedData =pkcs7.createEncryptedData(getFilesDir()+"", originalData, key);
		if(encryptedData==null){
			outputText +="\nEncrypedData is NUL\nL";
		}
		else{
			outputText +="\nCreating EncrypedData ... OK !\n";
			outputText += PlatformUtils.convertByteArrayToAscString(encryptedData);
		}
		output.setText(outputText);
		
		outputText+="\n Verifying Data ...";
		byte[] decryptedData = pkcs7.verifyEncrypedDate(getFilesDir()+"", encryptedData, key);
		if(decryptedData==null){
			outputText +="\nVerifying EncryptedData is failed";
		}
		else{
			outputText +="\n Verifiying Encrypted Data ... OK !";
			outputText += PlatformUtils.convertByteArrayToAscString(decryptedData);
		}
		output.setText(outputText);
		PlatformUtils.writeFile(getFilesDir()+"test.txt", "HelloWorld");
		outputText+="--- [Demo of the S/MIME functions]";
		SMIME smime = new SMIME();
		if((result =smime.createSMIMESignedData(getFilesDir()+"", subject1, password, getFilesDir()+"test.txt"))==0){
			outputText+="SMIME signed data successfully created in file: " +getFilesDir()+"test.txt";
			
		}
		else{
			outputText += "SMIME signing --- Failed!";
		}
		output.setText(outputText);
		}

	private void RevokeCert(ASN1Encoding encode) {
		Certificate myCertificate = new Certificate();
		int result;

		// --- usage parameter (Client certificate) --------------
		int digSignature = 128;
		int nonRepudiation = 64;
		int keyEncipherment = 32;
		int dataEncipherment = 16;
		int keyArrangement = 8;
		int keyCertSign = 4;
		int cRLSign = 2;

		int usage = keyCertSign + cRLSign;

		// --- Function 47 : revoke certificate in the Certificates DB -----
		//result = myCertificate.revoke(getFilesDir()+"", issuer, password, encode, 2);

		// --- Function 48 : list all certificates in the Certificates DB -----
		outputText+="\nListing all certificates from certificates.db ...";
		startingTime = System.currentTimeMillis();
		result = myCertificate.certificate_list(getFilesDir()+"");
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (result == 0){
			outputText +="\n********* Certificate DB listed";
		}
		else{
			outputText +="\n --------> Problems when listing certificates: "+result;
		}
		output.setText(outputText);

	}
	// ----------------------------------------------------------------------------------------
	public void ExtendCert(ASN1Encoding ASN1Certificate,	String password) {
		Certificate myCertificate = new Certificate();
		int result;

		// --- usage parameter (Client certificate) --------------
		int digSignature = 128;
		int nonRepudiation = 64;
		int keyEncipherment = 32;
		int dataEncipherment = 16;
		int keyArrangement = 8;
		int keyCertSign = 4;
		int cRLSign = 2;

		int usage = keyCertSign + cRLSign;
		// --- Function 49 : extend the lifetime of the certificate  -----
		outputText+="\n--- Function 49 : extend the lifetime of the certificate  -----";
		startingTime = System.currentTimeMillis();
		result = myCertificate.extend_Certificate(getFilesDir()+"", issuer, usage, password, ASN1Certificate);
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (result == 0){
			outputText +="\n********* Certificate extended";
		}
		else{
			outputText +="\n --------> Problems when extending Certificate result : "+result;
		}
		output.setText(outputText);

	}

	// ----------------------------------------------------------------------------------------
	public  void DelCert(DistinguishedName subject1) {
		Certificate myCertificate = new Certificate();
		int result;

		// --- usage parameter (Client certificate) --------------
		int digSignature = 128;
		int nonRepudiation = 64;
		int keyEncipherment = 32;
		int dataEncipherment = 16;
		int keyArrangement = 8;
		int keyCertSign = 4;
		int cRLSign = 2;

		int usage = digSignature + keyEncipherment + dataEncipherment;

		// --- Function 50 : delete certificate  -----
		outputText += "\n------ Function 50 : delete certificate  -----";
		startingTime = System.currentTimeMillis();
		result = myCertificate.delete(getFilesDir()+"", issuer, usage);
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (result == 0){
			outputText +="\n********* Certificate deleted";
		}
		else{
			outputText +="\n --------> Problems when deleting Certificate.... result : "+result;
		}
		output.setText(outputText);
	}

	public void GetPrivPubKeyCert(ASN1Encoding ASN1Certificate) {

		Certificate myCertificate = new Certificate(ASN1Certificate);
		PrivateKey privateKey;
		PublicKey publicKey;
		outputText += "\n--- Function 45 : Get Private Key from Certificate -----";
		startingTime = System.currentTimeMillis();
		// --- Function 45 : Get Private Key from Certificate -----
		privateKey = myCertificate.getPrivateKey(getFilesDir()+"", password);
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (privateKey !=null){
			outputText +="\n********* Private Key is " + privateKey.toString();
		}
		else{
			outputText +="\n -------->Get Private Key from certificate failed";
		}
		output.setText(outputText);

		//		--- Function 45 : Get PublicKey Key from Certificate -----
		outputText += "\n--- Function 45 : Get PublicKey from Certificate -----";
		startingTime = System.currentTimeMillis();
		publicKey = myCertificate.getPublicKey();
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (privateKey !=null){
			outputText +="\n********* Public Key is " + publicKey.toString();
		}
		else{
			outputText +="\n -------->Get Public Key from certificate failed";
		}
		output.setText(outputText);
		try {
			byte[] outputData = publicKey.encrypt(new byte[] {(byte)0x01});
			outputText +="\n -------->decrypted data is : " + outputData[0];
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchProviderException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		output.setText(outputText);

	}
	public void VerCert(ASN1Encoding ASN1Certificate) {

		Certificate myCertificate = new Certificate();
		int result;
		ASN1Encoding ASN1PublicKey = new ASN1Encoding();
		outputText += "\n--- Function 45 : Verify certificate -----";
		outputText+="****** 2 verifying Certificate";
		startingTime = System.currentTimeMillis();
		ASN1PublicKey = myCertificate.verify(getFilesDir()+"", ASN1Certificate);
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		Certificate newCertificate = new Certificate(ASN1Certificate);
		outputText+="****** 3 verifying Certificate";
		int retCode = newCertificate.verify(getFilesDir()+"");
		outputText+="****** Return code from verification " + retCode;
		if(ASN1PublicKey.value!=null){
			outputText+="****** Cerificate verified";
		}
		else{
			outputText+="****** Problems when verifying";
		}
		output.setText(outputText);
	}

	// ----------------------------------------------------------------------------------------
	public void SaveCert(ASN1Encoding ASN1Certificate) {
		Certificate myCertificate = new Certificate();
		int result;

		// --- Function 46 : save certificate in the Certificates DB -----
		outputText += "\n--- --- Function 46 : save certificate in the Certificates DB -----";

		startingTime = System.currentTimeMillis();
		result = myCertificate.save(getFilesDir()+"", ASN1Certificate);
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		if (result == 0){
			outputText +="\n********* Certificate saved";
		}
		else{
			outputText +="\n --------> Problems when saving Certificate.... result : "+result;
		}
		output.setText(outputText);
	}
	private void extractCertificate() {
		Certificate certificate = new Certificate();
		//Function 33 : get the first certificate from the Certificates DB

		Certificate myCertificate = certificate.getFirstCertificate(getFilesDir()+"");

		//Function 35 : list all X.509 certificate attributes
		String outputText = "Function 35 : list all X.509 certificate attributes";
		if (myCertificate != null)
		{
			outputText = "\n------------------------------------------------------------";

			int version = myCertificate.getVersion();
			long serialNumber = myCertificate.getSerialNumber().longValue();
			String signatureAlgorithm = myCertificate.getSignatureAlgorithm();
			Calendar validFrom = myCertificate.getValidFrom();
			Calendar validto = myCertificate.getValidTo();
			String publicKeyAlgorithm = myCertificate.getPublicKeyAlgorithm();
			DistinguishedName subjectDN = myCertificate.getSubject();
			DistinguishedName issuerDN = myCertificate.getIssuer();
			int keyUsage = myCertificate.getKeyUsage();
			String subjectAltName = myCertificate.getSubjectAltName();
			String issuerAltName = myCertificate.getIssuerAltName();
			boolean privateKeyUsagePeriod = myCertificate.verifyPrivateKeyUsagePeriod();
			String cAEntityType = myCertificate.getEntityType();
			String basicConstraintsType = myCertificate.getBasicConstraints_subjectType();
			int basicConstraintsPathLength = myCertificate.getBasicConstraints_pathLength();

			outputText+= "\nVersion                       : " + version+"\nSerial number                 : " + serialNumber +
					"\nSignature algorithm           : " + signatureAlgorithm+
					"\nValid from                    : " + 
					validFrom.get(1) + 
					" " + 
					validFrom.get(2) + 
					" " + 
					validFrom.get(5)+
					"/nValid to                      : " + 
					validto.get(1) + 
					" " + 
					validto.get(2) + 
					" " + 
					validto.get(5);
			output.setText(outputText);
			outputText+="\nPublic key algorithm          : " + publicKeyAlgorithm+
					"\nSubject (DN)                  : \n\n    " + subjectDN.toString() + "\n\n";

			outputText +="    Subject country is ............... : " + subjectDN.countryName;
			outputText +="\n    Subject state or province is ..... : " + subjectDN.stateOrProvinceName;
			outputText +="\n    Subject locality is .............. : " + subjectDN.localityName;
			outputText +="\n   Subject organization is .......... : " + subjectDN.organizationName;
			outputText +="\n    Subject organization unit is ..... : " + subjectDN.organizationalUnitName;
			outputText +="\n    Subject name is .................. : " + subjectDN.commonName;
			outputText +="\n   Subject email address is ......... : " + subjectDN.emailAddress;
			outputText +="\n    Subject url is ................... : " + subjectDN.urlAddress;

			output.setText(outputText);
			outputText +="Issuer (DN)                   : \n\n    " + issuerDN.toString() + "\n\n";

			outputText +="    Issuer country is ............... : " + issuerDN.countryName;
			outputText +="\n    Issuer state or province is ..... : " + issuerDN.stateOrProvinceName;
			outputText +="\n    Issuer locality is .............. : " + issuerDN.localityName;
			outputText +="\n    Issuer organization is .......... : " + issuerDN.organizationName;
			outputText +="\n    Issuer organization unit is ..... : " + issuerDN.organizationalUnitName;
			outputText +="\n    Issuer name is .................. : " + issuerDN.commonName;
			outputText +="\n    Issuer email address is ......... : " + issuerDN.emailAddress;
			outputText +="\n    Issuer url is ................... : " + issuerDN.urlAddress;
			outputText +="\nKey usage                     : " + keyUsage;
			output.setText(outputText);
			/*outputText +="\nSubject Alt Name              : \n\n    " + subjectAltName;
			outputText +="\nIssuer Alt Name               : \n\n    " + issuerAltName;*/
			if (privateKeyUsagePeriod) {
				outputText +="\nPrivate Key Usage Period      : verified ";
			}

			if (cAEntityType == null) {
				outputText +="CA Entity Type                : Not Found";
			}
			else {
				outputText +="CA Entity Type                : " + cAEntityType;
			}

			outputText +="Basic constraints type        : " + basicConstraintsType;
			outputText +="Basic constraints path length : " + basicConstraintsPathLength;

		}
	}
	protected void buildDNFLayout() {
		String outputHe = "";
		outputHe += " -- [ Demo of the DistinguishedName Functions ]\n"
				+ "Creating my DN";
		outputheader.setText(outputHe);
		//Function 26 : creation of the Distinguished Name
		String outputText = "Function 26 : creation of the Distinguished Name ...";
		startingTime = System.currentTimeMillis();
		DistinguishedName DN = new DistinguishedName("US", "DC", "Washington", "GWU", 
				"CSPRI", "Sead Muftic", "sead@dsv.su.se", "http://www.gwu.edu/ ~sead");
		endTime = System.currentTimeMillis();
		outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
		//Function 27 : extract Distinguished Name parameters
		outputText += "\n-----------------------------------------\n"+ 	
				"Function 27 : extract Distinguished Name parameters\n";
		outputText += "\nDN country is ........................... : "+ DN.countryName +
				"\nDN state or province is..................... : " + DN.stateOrProvinceName +
				"\nDN locality is ................" + DN.localityName + 
				"\nDN organization is ............" + DN.organizationName+
				"\nDN organization unit is ......." + DN.organizationalUnitName+
				"\nDN name is ............."+ DN.commonName+
				"\nDN email is ........." + DN.emailAddress;
		output.setText(outputText);


	}
	protected void buildAsymFunctionsLayout(int rSAType) {

		String outputHe = "\n" + rSAType +
				" ALL FUNCTIONS....";
		outputheader.setText(outputHe);
		String outputText = "Function 13 : Generate an asymmetric key pair ...";

		try {
			startingTime = System.currentTimeMillis();
			AsymmetricKeys defaultKeys= new AsymmetricKeys(rSAType);
			endTime = System.currentTimeMillis();
			outputText += "\n-----------------------------------------\n"+ 
					"Function 14 : Extract RSA Public Key from a pair of Asymmetric Keys ....\n"+
					"Function 15 : extract RSA Private Key From a pair of Asymmetric Keys\n"	;
			//Function 14 : Extract RSA Public Key from a pair of Asymmetric Keys ....
			PublicKey puk = defaultKeys.publicKey();
			//Function 15 : extract RSA Private Key From a pair of Asymmetric Keys
			PrivateKey prk = defaultKeys.privateKey();

			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += "\n Public key length : " +puk.getBytes().length;
			outputText += "\n Private key length :" +prk.getBytes().length;

			output.setText(outputText);

			outputText += "\n-----------------------------------------\n"+ 	
					"Function 16 : extract components of the RSA Public Key\n"+
					"Function 17 : extract CRT components of the RSA Private Key\n";

			startingTime= System.currentTimeMillis();
			//Function 16 : extract components of the RSA Public Key\n
			byte[] modulus = puk.get_N();
			byte[] exp = puk.get_E();

			//Function 17 : extract CRT components of the RSA Private Key
			byte[] P = prk.get_CRT_P();
			byte[] Q = prk.get_CRT_Q();
			byte[] PQ =prk.get_CRT_PQ();
			byte[] DP = prk.get_CRT_DP1();
			byte[] DQ = prk.get_CRT_DQ1();

			outputText += "Modulus length   : " + modulus.length + "\nExponent (e)   : " +exp.length
					+"/nP length   :" + P.length + "\nQ length   :" + Q.length + 
					"\nPQ length   :"+ PQ.length + "\nDP length   :"+DP.length +
					"\nDQ leghth   :"+DQ.length;

			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms\n";
			output.setText(outputText);

			outputText += "\n-----------------------------------------\n"+ 
					"Function 18 : encrypt message with RSA Public Key";
			startingTime= System.currentTimeMillis();
			encrypted = defaultKeys.publicKey().encrypt(plainText);
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += "\nOriginal Data : "+ plainText;
			outputText += "\nEncrypted Data : "+ encrypted;
			output.setText(outputText);

			outputText += "\n-----------------------------------------\n"+ "Function 19 : decrypt message with RSA Private Key ....\n";
			startingTime= System.currentTimeMillis();
			decrypted = defaultKeys.privateKey().decrypt(encrypted);
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += "\nOriginal Data : "+ plainText;
			outputText += "\nEncrypted Data : "+ encrypted;
			outputText += "\nDecrypted Data :" +decrypted;
			output.setText(outputText);

			//File pemFile=new File(getFilesDir(),"private.pem");
			outputText += "\n-----------------------------------------\n"+ 
					"Function  20 : Protecting (Encrypting) RSA private key with password and saving it to PEM file ....";

			startingTime= System.currentTimeMillis();
			String key=defaultKeys.privateKey().protectWithPassword(password);
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText +="\nProtected Key Data : \n"+ key;
			output.setText(outputText);

			outputText += "\n-----------------------------------------\n"+
					"Function  21 :  UnProtecting (Decrypting) it with password ....";
			startingTime= System.currentTimeMillis();
			PrivateKey unprotected=defaultKeys.privateKey().unprotectWithPassword(password);
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText +="\nUnrotected Key length: "+ unprotected.getBytes().length;
			output.setText(outputText);

			outputText += "\n-----------------------------------------\n"+ 
					"Function 23 : Enveloping Symmetric Key with Public Key ....\n";
			startingTime= System.currentTimeMillis();
			enveloped_symm_key = defaultKeys.publicKey().encrypt(serialized_symm_key);
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += "\nEnveloped Symmetric Key (String) \n:" + enveloped_symm_key;
			output.setText(outputText);

			outputText += "\n-----------------------------------------\n"+ 
					"Function 24 :De - Enveloping Symmetric Key with Public Key ....\n";
			startingTime = System.currentTimeMillis();
			deEnveloped_symm_key = defaultKeys.privateKey().decrypt(enveloped_symm_key);
			endTime=System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += "\nDe-Enveloped Symmetric Key (String) : \n " + deEnveloped_symm_key;
			output.setText(outputText);


			//Function 25 : creation of the original Symmetric Key object using serialized Symmetric Key
			outputText += "\n-----------------------------------------\n"+ 
					"Function 25 : creation of the original Symmetric Key object using serialized Symmetric Key ...\n";
			startingTime = System.currentTimeMillis();
			SymmetricKey deserialize = new SymmetricKey(serialized_symm_key);
			endTime=System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += "\nDe-Serialized Symmetric Key (String) : \n " + deserialize;
			output.setText(outputText);
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}
	protected void buildSymFunctionsLayout(int AesType) {
		String outputHe = "\n" + AesType +
				" ALL FUNCTIONS....";
		outputheader.setText(outputHe);
		String outputText = "Function 07 : Generate symmetric key and serialized ...\n";
		try {
			startingTime = System.currentTimeMillis();
			SymmetricKey key = new SymmetricKey(AesType);
			// --Function 22 : serialization of the Symmetric Key
			serialized_symm_key = key.serialize();
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += ("\nSerialized Symmetric Key : " +serialized_symm_key);
			output.setText(outputText);
			outputText += "\n-----------------------------------------\n"+ "Function 08 : Encrypt data using generated symmetric key ....\n";
			startingTime= System.currentTimeMillis();
			encrypted =key.encrypt(plainText);
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += "\nSS Key :" + serialized_symm_key;
			outputText += "\nOriginal Data : "+ plainText;
			outputText += "\nEncrypted Data : "+ encrypted;
			output.setText(outputText);

			outputText += "\n-----------------------------------------\n"+ "Function 09 : Decrypt data using generated symmetric key ....\n";
			startingTime= System.currentTimeMillis();
			decrypted = key.decrypt(encrypted);
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += "\nSS Key :" + serialized_symm_key;
			outputText += "\nOriginal Data : "+ plainText;
			outputText += "\nEncrypted Data : "+ encrypted;
			outputText += "\nDecrypted Data :" +decrypted;
			output.setText(outputText);


			outputText += "\n-----------------------------------------\n"+ "Function 10 : Generate salted symmetric key and serialized ...\n";
			startingTime= System.currentTimeMillis();
			SymmetricKey key2= new SymmetricKey(seed, -1, salt,Constants.AES);
			salted_symm_key = key2.serialize();
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += ("\nSerialized Salted Symmetric Key : " +salted_symm_key);
			output.setText(outputText);

			outputText += "\n-----------------------------------------\n"+ "Function 11 : Encrypt data using generated salted symmetric key ....\n";
			startingTime= System.currentTimeMillis();
			encrypted = key2.encrypt(plainText);
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += "\nSSS Key : \n" + salted_symm_key;
			outputText += "\nOriginal Data : "+ plainText;
			outputText += "\nEncrypted Data : "+ encrypted;
			output.setText(outputText);

			outputText += "\n-----------------------------------------\n"+ "Function 12 : Decrypt data using generated salted symmetric key ....\n";
			startingTime= System.currentTimeMillis();
			decrypted = key2.decrypt(encrypted);
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += "\nSSS Key :" + salted_symm_key;
			outputText += "\nOriginal Data : "+ plainText;
			outputText += "\nEncrypted Data : "+ encrypted;
			outputText += "\nDecrypted Data :" +decrypted;
			output.setText(outputText);
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
	private void buildHashFunctionsLayout(int type){
		Hash hash = new Hash();
		String outputHe = "\n" + type +
				" ALL FUNCTIONS....";
		outputheader.setText(outputHe);
		String outputText = "Function 01 : Create Hash of A String\n";

		try {
			startingTime = System.currentTimeMillis();
			byte[] hashData =hash.compute(type,plainText);
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			outputText += "\nOriginal String Data :\n"+ plainText+"\nHash(20 bytes/160 bits:\n"+ PlatformUtils.convertByteArrayToAscString(hashData);
			output.setText(outputText);

			outputText += "\n-----------------------------------------\n"+ "Function 02:Verify Hash of A String\n";
			startingTime= System.currentTimeMillis();
			int result = hash.verify(hashData,plainText);
			endTime=System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			if(result == 1){
				outputText+="\nVerification of the hash for a String ... OK !";
			}
			else {
				outputText+="Verification of the hash for a String ... Not OK !";
			}
			output.setText(outputText);
			//////////////////////////////////////////////////////////////////////
			outputText+= "\n-----------------------------------------\n"+ "Function 03 : Create Hash of A Binary Data\n";
			startingTime = System.currentTimeMillis();
			hashData =hash.compute(input_byte);
			endTime = System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime) + "ms";
			outputText += "\nOriginal Binary Data :\n"+ PlatformUtils.convertByteArrayToAscString(input_byte)+"\nHash(20 bytes/160 bits:\n"+ PlatformUtils.convertByteArrayToAscString(hashData);
			output.setText(outputText);


			outputText += "\n-----------------------------------------\n"+ "Function 04 : Verify Hash of Binary Data\n";
			startingTime= System.currentTimeMillis();
			result = hash.verify(hashData,input_byte);
			endTime=System.currentTimeMillis();
			outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
			if(result == 1){
				outputText+="\nVerification of the hash for binary data ... OK !";
			}
			else {
				outputText+="Verification of the hash for binary data ... Not OK !";
			}
			output.setText(outputText);
			///////////////////////////////////////////////////////////////
			outputText += "\n-----------------------------------------\n"+ "Function 05 : Create Hash of An Object\n";
			startingTime = System.currentTimeMillis();
			try {
				hashData =hash.compute(type,obj);
				endTime = System.currentTimeMillis();
				outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
				outputText += "\nOriginal Objectt :\n\n"+"\nHash(20 bytes/160 bits:\n"+ PlatformUtils.convertByteArrayToAscString(hashData);
				output.setText(outputText);


				outputText += "\n-----------------------------------------\n"+ "Function 06 : Verify Hash of An Object\n";
				startingTime= System.currentTimeMillis();
				result = hash.verify(obj, hashData,type);
				endTime=System.currentTimeMillis();
				outputText += "\nTotal Time :" + String.valueOf(endTime - startingTime)+ "ms";
				if(result == 1){
					outputText+="\nVerification of the hash for an Object ... OK !";
				}
				else {
					outputText+="Verification of the hash for an Object... Not OK !";
				}
				output.setText(outputText);	

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// -----------------------------------------------------------------------------------
	//     PKCS7 Functions
	// -----------------------------------------------------------------------------------
	public byte[] createSignDataPKCS7(String password) {


		PKCS7 pkcs7 = new PKCS7();
		DistinguishedName owner;

		// --- Create signedData -----------------------------------
	
		byte[] data =
			{(byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x04, (byte) 0x05, (byte) 0x06, (byte) 0x07, (byte) 0x08 };

		// --- Function 53 : create PKCS#7 SignedData -----
		byte[] signedData = pkcs7.createSignedData(getFilesDir()+"", data, subject1, password);

		if (signedData.length == 0)
			outputText +="\nProblem when creating PKCS#7 signedData ";
		else {
			outputText +="\nSuccess: PKCS#7 signedData successfully created!\n";
			outputText +=PlatformUtils.convertByteArrayToAscString(signedData);
		}
		return signedData;
	}

	public void verifySignDataPKCS7(String password, byte[] signedData) {


		PKCS7 pkcs7 = new PKCS7();
		// --- Function 55 : verify PKCS#7 SignedData -----
		int verificationCode = pkcs7.verifySignedData(getFilesDir()+"", signedData);
		if(verificationCode == 0)
			outputText +="\nSuccess: PKCS#7 signedData successfully verified";
		else
			outputText +="Problems when verifying PKCS#7 signedData ... Result is : " + verificationCode + "\n";

	}
	public byte[] addSignerPKCS7(String password, DistinguishedName anotherSigner, byte[] signedData, byte[] data) {


		PKCS7 pkcs7 = new PKCS7();
		// --- Function 54 : add additional Signer to PKCS#7 SignedData -----
		signedData = pkcs7.addSignerToDetachedSignedData(getFilesDir()+"", signedData, data, anotherSigner, password);
		if (signedData.length == 0)
			outputText +="\nProblem when adding PKCS#7 signedData ";
		else {
			outputText +="\nSuccess: PKCS#7 signature successfully added!\n\n";
			outputText +=PlatformUtils.convertByteArrayToAscString(signedData);
		}
		return signedData;
	}

	public void verifyPKCS7(String password, byte[] signedData) {
		PKCS7 pkcs7 = new PKCS7();
		// --- Function 55 : verify PKCS#7 SignedData -----
		int verificationCode = pkcs7.verifySignedData(getFilesDir()+"", signedData);
		if(verificationCode == 0)
			outputText +="\nSuccess: PKCS#7 signedData successfully verified";
		else
			outputText +="Problems when verifying PKCS#7 signedData ... Result is : " + verificationCode + "\n";

	}
	

}
